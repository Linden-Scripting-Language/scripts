// Task to solve: There is a prim with a navigation hud,
// listening on the channel 39. A product mentioned is from here:
// https://marketplace.secondlife.com/p/Wingsong-Space-Pod-WEAR-Box/4096211
// We need a script talking to it some speed,
// because the navigation hud will move the avatar then. 
// The navigation hud is NO MOD.
// Here we go - I want this hud to talk to channel 39 now.    

default

{

    touch_start(integer total_number) {

        // This should talk to channel 39 to a linked prim
llMessageLinked(1, 39, "1", "")
//We have syntax error on the next row!
    }

}
